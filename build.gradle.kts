plugins {
    kotlin("jvm") version "1.3.31"
}

repositories {
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.2.1")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-slf4j:1.2.1")


    implementation("org.slf4j:slf4j-api:1.7.26")
    runtimeOnly("ch.qos.logback:logback-classic:1.2.3")}
