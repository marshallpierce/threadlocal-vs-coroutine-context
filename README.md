An exploration of Java's `ThreadLocal` and Kotlin's `CoroutineContext` and how they work in common usage.

Example task to accomplish: when handling an http request with a request id, include that id in logs and in requests to downstream services so that all logs for one request across all services can be collected.
