package org.mpierce.threadlocal

import org.slf4j.LoggerFactory


val logger = LoggerFactory.getLogger("org.mpierce.threadlocal")!!


fun main() {
    val tl = ThreadLocal<String>()

    // value is null both on main thread ...
    logger.info("value is ${tl.get()}")

    // ... and on a new thread
    Thread {
        logger.info("value is ${tl.get()}")
    }.apply {
        start()
        join()
    }

    tl.set("Set from main thread!")

    // not set on a new thread
    Thread {
        logger.info("value is ${tl.get()}")
    }.apply {
        start()
        join()
    }

    // but it is on main
    logger.info("value is ${tl.get()} in ${Thread.currentThread().name}")
}
