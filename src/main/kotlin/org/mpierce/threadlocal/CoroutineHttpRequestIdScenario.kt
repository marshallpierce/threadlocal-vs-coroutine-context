package org.mpierce.threadlocal

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.slf4j.MDCContext
import kotlinx.coroutines.withContext
import org.slf4j.MDC
import java.time.Duration
import java.time.Instant
import java.util.concurrent.ThreadLocalRandom
import kotlin.coroutines.AbstractCoroutineContextElement
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.coroutineContext

/**
 * Toy scenario: http request handling logic as in a typical server side app. Requests have a request id header
 * (provided by the load balancer, let's say) and you want to propagate that header to all downstream requests you make
 * so that you could look at logs for all logic for this request across multiple services
 */
fun main() {
    runBlocking {
        // run coroutines on the coroutine default thread pool
        withContext(Dispatchers.Default) {
            val requestId = "abc123"

            // make a slf4j mdc map with the request id
            val mdcWithRequest = (MDC.getCopyOfContextMap() ?: mapOf())
                    .plus<String, String>(Pair("requestId", requestId))
            // build a coroutine context that includes structured HttpRequestId element as well as MDC
            withContext(HttpRequestId(requestId) +
                    MDCContext(mdcWithRequest)) {
                handleHttpRequestCoroutine()
            }
        }
    }
}

private suspend fun handleHttpRequestCoroutine() {
    logger.info("Doing important business logic")

    coroutineScope {
        val dataFromOtherService = async {
            val data = callSomeOtherServiceCoroutine()
            logger.info(
                    "Request ${coroutineContext[HttpRequestId]?.id} produced ${data.length} bytes of data")

            data.length
        }

        val dbData = async {
            val records = getDataFromDatabase()
            logger.info("Postprocessing db results for request ${coroutineContext[HttpRequestId]?.id}")

            records.map { it.capitalize() }
        }

        logger.info("All done: ${dataFromOtherService.await()} ${dbData.await()}")
    }

}

private suspend fun callSomeOtherServiceCoroutine(): String {
    val start = Instant.now()

    logger.info("Making call to other service with requestId=${coroutineContext[HttpRequestId]?.id}")

    // here you could also just `await()` on a CompletableFuture if that's what your http client exposes
    delay(1000)

    logger.debug(
            "Call for request id ${coroutineContext[HttpRequestId]?.id} took ${Duration.between(start, Instant.now())}")

    return "important data ".repeat(1 + ThreadLocalRandom.current().nextInt(5))
}

private suspend fun getDataFromDatabase(): List<String> {
    // suppose you need to make a call with blocking i/o like JDBC
    return withContext(Dispatchers.IO) {
        logger.info("Doing db query for requestId=${coroutineContext[HttpRequestId]?.id}")

        // simulate blocking i/o
        Thread.sleep(1000)

        (0..1 + ThreadLocalRandom.current().nextInt(5)).map { "db data" }
    }
}

data class HttpRequestId(val id: String) : AbstractCoroutineContextElement(HttpRequestId) {
    companion object Key : CoroutineContext.Key<HttpRequestId>
}
