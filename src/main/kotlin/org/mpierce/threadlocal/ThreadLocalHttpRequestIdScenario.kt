package org.mpierce.threadlocal

import org.slf4j.MDC
import java.time.Duration
import java.time.Instant
import java.util.Locale
import java.util.concurrent.Callable
import java.util.concurrent.CompletableFuture
import java.util.concurrent.Executors
import java.util.concurrent.ThreadLocalRandom

private val currentRequestId = ThreadLocal<String>()
private val pool = Executors.newCachedThreadPool()

/**
 * Toy scenario: http request handling logic as in a typical server side app. Requests have a request id header
 * (provided by the load balancer, let's say) and you want to propagate that header to all downstream requests you make
 * so that you could look at logs for all logic for this request across multiple services
 */
fun main() {
    // parse from http request headers or whatever
    val requestId = "abc123"
    MDC.put("httpRequestId", requestId)
    currentRequestId.set(requestId)
    try {
        handleHttpRequest()
    } finally {
        MDC.remove("httpRequestId")
        currentRequestId.remove()
    }

    pool.shutdown()
}

private fun handleHttpRequest() {
    logger.info("Doing important business logic")

    val dataFromOtherService = callSomeOtherService()
            .thenApplyAsync { data ->
                logger.info("Request ${currentRequestId.get()} produced ${data.length} bytes of data")

                // whatever post processing you need to do
                data.toUpperCase(Locale.US)
            }

    val dataFromDatabase = pool.submit(Callable {
        getDataFromDatabase()
                .let { records ->
                    logger.info("Postprocessing db results for request ${currentRequestId.get()}")

                    records.map { it.capitalize() }
                }
    })

    logger.info("Doing other work while waiting for the database...")

    logger.info("All done: ${dataFromOtherService.get()} ${dataFromDatabase.get()}")
}

private fun callSomeOtherService(): CompletableFuture<String> {
    val future = CompletableFuture<String>()

    // attach a callback for monitoring or anything else you want to do
    val start = Instant.now()
    future.whenComplete { _, _ ->
        logger.debug("Call for request id ${currentRequestId.get()} took ${Duration.between(start, Instant.now())}")
    }

    logger.info("Making call to other service with requestId=${currentRequestId.get()}")

    // fake nonblocking i/o wait by completing the future in the background
    pool.submit {
        Thread.sleep(1000)
        future.complete("important data ".repeat(1 + ThreadLocalRandom.current().nextInt(5)))
    }

    return future
}

private fun getDataFromDatabase(): List<String> {
    logger.info("Running db query for requestId=${currentRequestId.get()}")

    // simulate blocking i/o
    Thread.sleep(1000)
    return ((0..1 + ThreadLocalRandom.current().nextInt(5)).map { "db data" })
}
