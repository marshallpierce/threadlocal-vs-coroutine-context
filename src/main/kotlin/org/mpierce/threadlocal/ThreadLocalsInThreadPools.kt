package org.mpierce.threadlocal

import java.util.concurrent.Executors

fun main() {
    val pool = Executors.newCachedThreadPool()

    val tl = ThreadLocal<String>()
    tl.set("foo")

    // no value here -- pool thread != main thread
    pool.submit {
        logger.info("value is ${tl.get()}")
    }.get()

    // set it on presumably the first pool thread
    pool.submit {
        logger.info("Setting threadlocal")
        tl.set("bar")
    }.get()

    // submit a long running task -- will see the value set above assuming it uses the same underlying thread
    pool.submit {
        logger.info("value is ${tl.get()}")
        Thread.sleep(1000)
    }

    // a different long running task will create a new thread, and therefore will not get the threadlocal
    pool.submit {
        logger.info("value is ${tl.get()}")
        Thread.sleep(1000)
    }

    pool.shutdown()
}
