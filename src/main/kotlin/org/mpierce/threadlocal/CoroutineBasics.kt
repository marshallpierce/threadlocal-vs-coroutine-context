package org.mpierce.threadlocal

import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() {
    runBlocking {
        val j1 = launch {
            delay(1000)
            logger.info("job 1 done")
        }

        val j2 = launch {
            delay(1000)
            logger.info("job 2 done")
        }

        logger.info("waiting for jobs")
        j1.join()
        j2.join()
        logger.info("all jobs done")
    }
}
