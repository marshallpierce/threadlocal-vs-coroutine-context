package org.mpierce.threadlocal

import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import kotlin.coroutines.coroutineContext

fun main() {
    runBlocking {
        withContext(Dispatchers.Default) {
            useCoroutineContexts()
        }
    }
}

private suspend fun useCoroutineContexts() {
    logger.info("Running in coroutine ${coroutineContext[CoroutineName]}")

    withContext(CoroutineName("foo")) {
        logger.info("Now in coroutine ${coroutineContext[CoroutineName]}")

        launch {
            delay(1000)
            logger.info("Launched from ${coroutineContext[CoroutineName]}")
        }

        launch {
            delay(1000)
            logger.info("Also launched from ${coroutineContext[CoroutineName]}")
        }
    }

    logger.info("Still running in coroutine ${coroutineContext[CoroutineName]}")
}
