package org.mpierce.threadlocal

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.asContextElement
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.util.concurrent.Executors

private val pool = Executors.newSingleThreadExecutor()

fun main() {
    runBlocking {
        withContext(Dispatchers.Default) {
            useThreadContext()
        }
    }

    pool.shutdown()
}

private suspend fun useThreadContext() {
    val tl = ThreadLocal<String>()
    tl.set("foo")

    logger.info("value: ${tl.get()}")

    // a dispatcher that is definitely in another thread
    val dispatcher = pool.asCoroutineDispatcher()

    withContext(dispatcher) {
        logger.info("value: ${tl.get()}")
    }

    // now, with tl as thread context
    withContext(tl.asContextElement() + dispatcher) {
        logger.info("value: ${tl.get()}")
    }

    // tl is not set now because it was unset above at the end of the coroutine's execution
    withContext(dispatcher) {
        logger.info("value: ${tl.get()}")
    }

}
