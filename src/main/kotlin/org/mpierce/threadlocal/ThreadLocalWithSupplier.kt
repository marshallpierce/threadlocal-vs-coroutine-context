package org.mpierce.threadlocal

import java.util.concurrent.atomic.AtomicInteger
import java.util.function.Supplier

fun main() {
    // you can provide a Supplier that sets the new value used for each thread. Here, we keep some state to provide
    // unique ids per thread.
    val tl = ThreadLocal.withInitial(object : Supplier<Int> {
        private val counter = AtomicInteger()

        override fun get(): Int {
            return counter.getAndIncrement()
        }
    })

    logger.info("value is ${tl.get()}")

    Thread {
        logger.info("value is ${tl.get()}")
    }.apply {
        start()
        join()
    }

    logger.info("value is ${tl.get()}")
}
